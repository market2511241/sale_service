package service

import (
	"context"
	"market/sale_service/config"
	"market/sale_service/gRPC/client"
	"market/sale_service/genproto/sale_service"
	"market/sale_service/pkg/logger"
	"market/sale_service/storage"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type SmenaService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*sale_service.UnimplementedSaleServiceServer
}

func NewSmenaService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *SmenaService {
	return &SmenaService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *SmenaService) Create(ctx context.Context, req *sale_service.SmenaCreateReq) (*sale_service.SmenaCreateResp, error) {
	u.log.Info("====== Smena Create ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating Smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) GetList(ctx context.Context, req *sale_service.SmenaGetListReq) (*sale_service.SmenaGetListResp, error) {
	u.log.Info("====== Smena GetList ======", logger.Any("req", req))

	resp, err := u.strg.Smena().GetList(ctx, req)
	if err != nil {
		u.log.Error("error while getting Smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) GetById(ctx context.Context, req *sale_service.SmenaIdReq) (*sale_service.Smena, error) {
	u.log.Info("====== Smena GetById ======", logger.Any("req", req))

	resp, err := u.strg.Smena().GetById(ctx, req)
	if err != nil {
		u.log.Error("error while getting Smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) Update(ctx context.Context, req *sale_service.SmenaUpdateReq) (*sale_service.SmenaUpdateResp, error) {
	u.log.Info("====== Smena Update ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Update(ctx, req)
	if err != nil {
		u.log.Error("error while updating Smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) Delete(ctx context.Context, req *sale_service.SmenaIdReq) (*sale_service.SmenaDeleteResp, error) {
	u.log.Info("====== Smena Delete ======", logger.Any("req", req))

	resp, err := u.strg.Smena().Delete(ctx, req)
	if err != nil {
		u.log.Error("error while deleting Smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *SmenaService) CheckSmena(ctx context.Context, req *sale_service.SmenaCheckReq) (*sale_service.SmenaCheckResp, error) {
	u.log.Info("====== Smena Check ======", logger.Any("req", req))

	resp, err := u.strg.Smena().CheckSmena(ctx, req)
	if err != nil {
		u.log.Error("error while deleting smena", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
