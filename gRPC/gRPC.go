package grpc

import (
	"market/sale_service/config"
	"market/sale_service/gRPC/client"
	"market/sale_service/gRPC/service"
	"market/sale_service/genproto/sale_service"
	"market/sale_service/pkg/logger"
	"market/sale_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {
	grpcServer = grpc.NewServer()

	sale_service.RegisterSaleServiceServer(grpcServer, service.NewSaleService(cfg, log, strg, srvc))
	sale_service.RegisterSmenaServiceServer(grpcServer, service.NewSmenaService(cfg, log, strg, srvc))
	sale_service.RegisterSaleProductServiceServer(grpcServer, service.NewSaleProductService(cfg, log, strg, srvc))
	sale_service.RegisterStaffTransactionServiceServer(grpcServer, service.NewStaffTransactionService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)

	return
}
