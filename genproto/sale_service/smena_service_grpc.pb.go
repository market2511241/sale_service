// Code generated by protoc-gen-go-grpc. DO NOT EDIT.
// versions:
// - protoc-gen-go-grpc v1.2.0
// - protoc             v3.12.4
// source: smena_service.proto

package sale_service

import (
	context "context"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
// Requires gRPC-Go v1.32.0 or later.
const _ = grpc.SupportPackageIsVersion7

// SmenaServiceClient is the client API for SmenaService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://pkg.go.dev/google.golang.org/grpc/?tab=doc#ClientConn.NewStream.
type SmenaServiceClient interface {
	Create(ctx context.Context, in *SmenaCreateReq, opts ...grpc.CallOption) (*SmenaCreateResp, error)
	GetList(ctx context.Context, in *SmenaGetListReq, opts ...grpc.CallOption) (*SmenaGetListResp, error)
	GetById(ctx context.Context, in *SmenaIdReq, opts ...grpc.CallOption) (*Smena, error)
	Update(ctx context.Context, in *SmenaUpdateReq, opts ...grpc.CallOption) (*SmenaUpdateResp, error)
	Delete(ctx context.Context, in *SmenaIdReq, opts ...grpc.CallOption) (*SmenaDeleteResp, error)
	CheckSmena(ctx context.Context, in *SmenaCheckReq, opts ...grpc.CallOption) (*SmenaCheckResp, error)
}

type smenaServiceClient struct {
	cc grpc.ClientConnInterface
}

func NewSmenaServiceClient(cc grpc.ClientConnInterface) SmenaServiceClient {
	return &smenaServiceClient{cc}
}

func (c *smenaServiceClient) Create(ctx context.Context, in *SmenaCreateReq, opts ...grpc.CallOption) (*SmenaCreateResp, error) {
	out := new(SmenaCreateResp)
	err := c.cc.Invoke(ctx, "/sale_service.SmenaService/Create", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *smenaServiceClient) GetList(ctx context.Context, in *SmenaGetListReq, opts ...grpc.CallOption) (*SmenaGetListResp, error) {
	out := new(SmenaGetListResp)
	err := c.cc.Invoke(ctx, "/sale_service.SmenaService/GetList", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *smenaServiceClient) GetById(ctx context.Context, in *SmenaIdReq, opts ...grpc.CallOption) (*Smena, error) {
	out := new(Smena)
	err := c.cc.Invoke(ctx, "/sale_service.SmenaService/GetById", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *smenaServiceClient) Update(ctx context.Context, in *SmenaUpdateReq, opts ...grpc.CallOption) (*SmenaUpdateResp, error) {
	out := new(SmenaUpdateResp)
	err := c.cc.Invoke(ctx, "/sale_service.SmenaService/Update", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *smenaServiceClient) Delete(ctx context.Context, in *SmenaIdReq, opts ...grpc.CallOption) (*SmenaDeleteResp, error) {
	out := new(SmenaDeleteResp)
	err := c.cc.Invoke(ctx, "/sale_service.SmenaService/Delete", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *smenaServiceClient) CheckSmena(ctx context.Context, in *SmenaCheckReq, opts ...grpc.CallOption) (*SmenaCheckResp, error) {
	out := new(SmenaCheckResp)
	err := c.cc.Invoke(ctx, "/sale_service.SmenaService/CheckSmena", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// SmenaServiceServer is the server API for SmenaService service.
// All implementations should embed UnimplementedSmenaServiceServer
// for forward compatibility
type SmenaServiceServer interface {
	Create(context.Context, *SmenaCreateReq) (*SmenaCreateResp, error)
	GetList(context.Context, *SmenaGetListReq) (*SmenaGetListResp, error)
	GetById(context.Context, *SmenaIdReq) (*Smena, error)
	Update(context.Context, *SmenaUpdateReq) (*SmenaUpdateResp, error)
	Delete(context.Context, *SmenaIdReq) (*SmenaDeleteResp, error)
	CheckSmena(context.Context, *SmenaCheckReq) (*SmenaCheckResp, error)
}

// UnimplementedSmenaServiceServer should be embedded to have forward compatible implementations.
type UnimplementedSmenaServiceServer struct {
}

func (UnimplementedSmenaServiceServer) Create(context.Context, *SmenaCreateReq) (*SmenaCreateResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Create not implemented")
}
func (UnimplementedSmenaServiceServer) GetList(context.Context, *SmenaGetListReq) (*SmenaGetListResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetList not implemented")
}
func (UnimplementedSmenaServiceServer) GetById(context.Context, *SmenaIdReq) (*Smena, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetById not implemented")
}
func (UnimplementedSmenaServiceServer) Update(context.Context, *SmenaUpdateReq) (*SmenaUpdateResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Update not implemented")
}
func (UnimplementedSmenaServiceServer) Delete(context.Context, *SmenaIdReq) (*SmenaDeleteResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method Delete not implemented")
}
func (UnimplementedSmenaServiceServer) CheckSmena(context.Context, *SmenaCheckReq) (*SmenaCheckResp, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CheckSmena not implemented")
}

// UnsafeSmenaServiceServer may be embedded to opt out of forward compatibility for this service.
// Use of this interface is not recommended, as added methods to SmenaServiceServer will
// result in compilation errors.
type UnsafeSmenaServiceServer interface {
	mustEmbedUnimplementedSmenaServiceServer()
}

func RegisterSmenaServiceServer(s grpc.ServiceRegistrar, srv SmenaServiceServer) {
	s.RegisterService(&SmenaService_ServiceDesc, srv)
}

func _SmenaService_Create_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmenaCreateReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SmenaServiceServer).Create(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale_service.SmenaService/Create",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SmenaServiceServer).Create(ctx, req.(*SmenaCreateReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _SmenaService_GetList_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmenaGetListReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SmenaServiceServer).GetList(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale_service.SmenaService/GetList",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SmenaServiceServer).GetList(ctx, req.(*SmenaGetListReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _SmenaService_GetById_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmenaIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SmenaServiceServer).GetById(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale_service.SmenaService/GetById",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SmenaServiceServer).GetById(ctx, req.(*SmenaIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _SmenaService_Update_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmenaUpdateReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SmenaServiceServer).Update(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale_service.SmenaService/Update",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SmenaServiceServer).Update(ctx, req.(*SmenaUpdateReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _SmenaService_Delete_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmenaIdReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SmenaServiceServer).Delete(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale_service.SmenaService/Delete",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SmenaServiceServer).Delete(ctx, req.(*SmenaIdReq))
	}
	return interceptor(ctx, in, info, handler)
}

func _SmenaService_CheckSmena_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(SmenaCheckReq)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(SmenaServiceServer).CheckSmena(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/sale_service.SmenaService/CheckSmena",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(SmenaServiceServer).CheckSmena(ctx, req.(*SmenaCheckReq))
	}
	return interceptor(ctx, in, info, handler)
}

// SmenaService_ServiceDesc is the grpc.ServiceDesc for SmenaService service.
// It's only intended for direct use with grpc.RegisterService,
// and not to be introspected or modified (even as a copy)
var SmenaService_ServiceDesc = grpc.ServiceDesc{
	ServiceName: "sale_service.SmenaService",
	HandlerType: (*SmenaServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "Create",
			Handler:    _SmenaService_Create_Handler,
		},
		{
			MethodName: "GetList",
			Handler:    _SmenaService_GetList_Handler,
		},
		{
			MethodName: "GetById",
			Handler:    _SmenaService_GetById_Handler,
		},
		{
			MethodName: "Update",
			Handler:    _SmenaService_Update_Handler,
		},
		{
			MethodName: "Delete",
			Handler:    _SmenaService_Delete_Handler,
		},
		{
			MethodName: "CheckSmena",
			Handler:    _SmenaService_CheckSmena_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "smena_service.proto",
}
