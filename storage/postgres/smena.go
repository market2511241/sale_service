package postgres

import (
	"context"
	"fmt"
	"market/sale_service/genproto/sale_service"

	"github.com/google/uuid"
	"github.com/jackc/pgx"
	"github.com/jackc/pgx/v5/pgxpool"
)

type SmenaRepo struct {
	db *pgxpool.Pool
}

func NewSmenaRepo(db *pgxpool.Pool) *SmenaRepo {
	return &SmenaRepo{
		db: db,
	}
}

func (r *SmenaRepo) Create(ctx context.Context, req *sale_service.SmenaCreateReq) (*sale_service.SmenaCreateResp, error) {
	id := uuid.NewString()

	query := `
	INSERT INTO smena(
		id,
		staff_id,
		branch_id,
		sale_amount,
		status
	)
	VALUES($1,$2,$3,$4,$5);
	`

	_, err := r.db.Exec(ctx, query,
		id,
		req.StaffId,
		req.BranchId,
		req.SaleAmount,
		req.Status,
	)

	if err != nil {
		fmt.Println("error:", err.Error())
		return nil, err
	}

	return &sale_service.SmenaCreateResp{
		Msg: "smena created with id: " + id,
	}, nil
}

func (r *SmenaRepo) GetList(ctx context.Context, req *sale_service.SmenaGetListReq) (*sale_service.SmenaGetListResp, error) {
	var (
		filter  = " WHERE deleted_at IS NULL "
		offsetQ = " OFFSET 0;"
		limit   = " LIMIT 10 "
		offset  = (req.Page - 1) * req.Limit
		count   int
	)

	s := `
	SELECT 
		id,
		staff_id,
		branch_id,
		sale_amount,
		status,
		created_at::TEXT,
		finished_at::TEXT 
	FROM 
		smena 
	`
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}

	query := s + filter + limit + offsetQ
	// fmt.Println(query)

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	resp := &sale_service.SmenaGetListResp{}
	for rows.Next() {
		var smena = sale_service.Smena{}
		err := rows.Scan(
			&smena.Id,
			&smena.StaffId,
			&smena.BranchId,
			&smena.SaleAmount,
			&smena.Status,
			&smena.CreatedAt,
			&smena.FinishedAt,
		)

		if err != nil {
			return nil, err
		}
		resp.Smenas = append(resp.Smenas, &smena)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *SmenaRepo) GetById(ctx context.Context, req *sale_service.SmenaIdReq) (*sale_service.Smena, error) {
	query := `
	SELECT 
		id,
		staff_id,
		branch_id,
		sale_amount,
		status,
		created_at::TEXT,
		finished_at::TEXT 
	FROM 
		smena 
	WHERE 
		id=$1
	`

	var smena = sale_service.Smena{}
	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&smena.Id,
		&smena.StaffId,
		&smena.BranchId,
		&smena.SaleAmount,
		&smena.Status,
		&smena.CreatedAt,
		&smena.FinishedAt,
	); err != nil {
		return nil, err
	}

	return &smena, nil
}

func (r *SmenaRepo) Update(ctx context.Context, req *sale_service.SmenaUpdateReq) (*sale_service.SmenaUpdateResp, error) {
	query := `
	UPDATE smena 
	SET 
		staff_id=$2,
		branch_id=$3,
		sale_amount=$4,
		status=$5,
	WHERE id=$1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
		req.StaffId,
		req.BranchId,
		req.SaleAmount,
		req.Status,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &sale_service.SmenaUpdateResp{Msg: "OK"}, nil
}

func (r *SmenaRepo) Delete(ctx context.Context, req *sale_service.SmenaIdReq) (*sale_service.SmenaDeleteResp, error) {
	query := `
    DELETE FROM 
		smena 
    WHERE 
		id=$;
	`
	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}
	if res.RowsAffected() == 0 {
		return nil, pgx.ErrNoRows
	}

	return &sale_service.SmenaDeleteResp{Msg: "OK"}, nil
}

func (r *SmenaRepo) CheckSmena(ctx context.Context, req *sale_service.SmenaCheckReq) (*sale_service.SmenaCheckResp, error) {
	var count int
	query := `
	SELECT COUNT(*) FROM smena
	WHERE staff_id = $1 AND branch_id = $2 AND status='opened';`

	if err := r.db.QueryRow(ctx, query, req.StaffId, req.BranchId).Scan(&count); err != nil {
		return nil, err
	}

	if count > 0 {
		return &sale_service.SmenaCheckResp{Msg: "true"}, nil
	} else {
		return &sale_service.SmenaCheckResp{Msg: "false"}, nil
	}
}