CREATE TYPE "st_tr_type" AS ENUM (
  'withdraw',
  'topup'
);

CREATE TYPE "st_tr_source_type" AS ENUM (
  'sales',
  'bonus'
);

CREATE TYPE "sale_payment_type" AS ENUM (
  'card',
  'cash'
);

CREATE TYPE "sale_status" AS ENUM (
  'new',
  'finished',
  'cancel'
);

CREATE TYPE "br_pr_tr_type" AS ENUM (
  'plus',
  'minus'
);

CREATE TYPE "smena_status" AS ENUM (
  'opened',
  'closed'
);

CREATE TABLE "staff_transactions" (
  "id" uuid PRIMARY KEY,
  "sale_id" uuid,
  "staff_id" uuid,
  "type" st_tr_type,
  "source_type" st_tr_source_type,
  "amount" numeric,
  "about_text" text,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "smena" (
  "id" uuid PRIMARY KEY,
  "staff_id" uuid,
  "branch_id" uuid,
  "sale_amount" float,
  "status" smena_status,
  "created_at" timestamp DEFAULT (now()),
  "finished_at" timestamp DEFAULT (now())
);

CREATE TABLE "sales" (
  "id" uuid PRIMARY KEY,
  "branch_id" uuid,
  "smena_id" uuid,
  "shop_assistant_id" uuid,
  "cashier_id" uuid,
  "price" numeric,
  "payment_type" sale_payment_type,
  "status" sale_status,
  "client_name" varchar(255),
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

CREATE TABLE "sale_products" (
  "sale_id" uuid,
  "product_id" uuid,
  "quantity" integer,
  "price" numeric
);

CREATE TABLE "branch_product_transactions" (
  "id" uuid PRIMARY KEY,
  "branch_id" uuid,
  "staff_id" uuid,
  "product_id" uuid,
  "price" numeric,
  "type" br_pr_tr_type,
  "quantity" integer,
  "created_at" timestamp DEFAULT (now()),
  "updated_at" timestamp DEFAULT (now()),
  "deleted_at" timestamp
);

ALTER TABLE "staff_transactions" ADD FOREIGN KEY ("sale_id") REFERENCES "sales" ("id");

ALTER TABLE "sale_products" ADD FOREIGN KEY ("sale_id") REFERENCES "sales" ("id");

ALTER TABLE "sales" ADD FOREIGN KEY ("id") REFERENCES "smena" ("id");